import numpy as np
import matplotlib.pyplot as plt


# Load covariance matrix and data
bins = 31
cov_matrix = np.genfromtxt("/home/gitaj/gitlab/MCMC/jla_mub_covmatrix.txt")
data = np.genfromtxt("/home/gitaj/gitlab/MCMC/jla_mub_0.txt")
z = data[:, 0]
m = data[:, 1]
cov_inverse = np.linalg.inv(np.reshape(cov_matrix, (bins, bins)))

# Define functions for calculations
def eta(a, omega):
    # Calculate eta
    omega = np.clip(omega, 0.0, 1.0)
    s = ((1 - omega) / omega) ** (1 / 3)
    n = 2.0 * (np.sqrt((s ** 3) + 1)) * (((a ** (-4)) - (0.154 * s * (a ** (-3))) + (
            0.4304 * s * s * (a ** (-2))) + (0.19097 * (s ** 3) * (a ** (-1))) + (
                                                    0.066941 * s ** 4))) ** (-1.0 / 8.0)
    return n

def dl_function(z, omega):
    # Calculate luminosity distance
    eta_1 = eta(1, omega)
    eta_2 = eta(1 / (1 + z), omega)
    return (3000 * (1 + z)) * (eta_1 - eta_2)

def mu_function(z, omega, hubble):
    # Calculate distance modulus
    return 25 - (5 * np.log10(hubble)) + (5 * np.log10(dl_function(z, omega)))


def mh_mcmc(sigma_omega, sigma_h, omega_es, hubble_es, num_samples,plot):
    # Parameters and initial values
    bins = 31
    omega_values = [omega_es]
    hubble_values = [hubble_es]


    def log_likelihood(omega, hubble, z, m):
        # Calculate likelihood
        if omega <= 0.0 or hubble <= 0.0:
            return -1e100
        else:
            diff = m - mu_function(z, omega, hubble)
            dot_product = np.dot(cov_inverse, diff)
            return -0.5 * np.dot(diff, dot_product)



    def metropolis_hastings(omega_0, hubble_0):
        # Metropolis-Hastings MCMC step
        omega_1 = np.random.normal(omega_0, sigma_omega)
        hubble_1 = np.random.normal(hubble_0, sigma_h)


        p_old = log_likelihood(omega_0, hubble_0, z, m)
        p_new = log_likelihood(omega_1, hubble_1, z, m)

        alpha = p_new - p_old
        if np.log(np.random.rand()) < alpha:
            return omega_1, hubble_1, True
        else:
            return omega_0, hubble_0, False

    # MCMC sampling
    acceptance_count = 0
    likelihoods = []
    for _ in range(1, num_samples):
        omega_0, hubble_0 = omega_values[-1], hubble_values[-1]
        omega_1, hubble_1, accept = metropolis_hastings(omega_0, hubble_0)
        if accept:
            omega_values.append(omega_1)
            hubble_values.append(hubble_1)
            acceptance_count += 1

            likelihoods.append(np.exp(log_likelihood(omega_1, hubble_1, z, m)))

             
    # Burn in period
    burn = 0
    for i in range(0,len(likelihoods)):
       if likelihoods[i] < (1/np.e)*np.max(likelihoods):  # Rejecting values 
          burn += 1
       else:
          break

               

    # Discard burn-in samples
    omega_final = omega_values[burn:]
    hubble_final = hubble_values[burn:]
    likelihoods_final = likelihoods[burn:]

    # Analysis and output after burn-in
    omega_mean = np.mean(omega_final)
    hubble_mean = np.mean(hubble_final)

    omega_mean = np.mean(omega_final)
    hubble_mean = np.mean(hubble_final)

 
    if plot == True:

        theoretical_modulus = np.empty(bins)

        for i in range(bins):

         theoretical_modulus[i] = mu_function(z[i], omega_mean, hubble_mean)
   
        # Print results

        print('MH MCMC without prior')

        print('Estimated matter density: ', omega_mean)
        print('Estimated hubble scaling constant: ', hubble_mean)
        print('Acceptance percentage: ', (acceptance_count * 100) / num_samples)
        print('Burn in percentage:',(len(hubble_values)-len(hubble_final))*100/len(hubble_values))





        # Plotting

        plt.figure(figsize=[10,6])
        plt.xlabel('Redshift (z)')
        plt.ylabel('Distance Modulus')
        
        plt.title('Data vs. Theoretical Model (without prior)')
        plt.plot(z, m,'o', label='Given Data')
        plt.plot(z, theoretical_modulus,'--', label="Theoretical Model from Estimates")
        plt.legend()
        plt.grid()
        plt.show()


        plt.figure(figsize=[14, 10])
        
        plt.suptitle("MH-MCMC without the prior",size=18)
        
        # Scatter plot for Metropolis-Hastings MCMC samples
        plt.subplot(221)
        plt.scatter(omega_values[1:], hubble_values[1:], c=likelihoods)
        cbar = plt.colorbar()
        cbar.set_label('Likelihood')
        plt.title('MH-MCMC Random Walk')
        plt.xlabel('Matter Density')
        plt.ylabel('Hubble Constant')
        plt.grid()

        
        plt.subplot(222)
        plt.scatter(omega_final[1:], hubble_final[1:], c=likelihoods_final)
        cbar = plt.colorbar()
        cbar.set_label('Likelihood')
        
        plt.title('Samples after Burn In Period')
        plt.xlabel('Matter Density')
        plt.ylabel('Hubble Constant')
        plt.grid()

        
        plt.subplot(223)
        plt.hist(omega_final, bins=40, edgecolor='black')
        plt.title("Histogram for Matter Density")

        
        plt.subplot(224)
        plt.hist(hubble_final, bins=40, edgecolor='black')
        plt.title("Histogram for Hubble Scaling Constant")

        
        plt.tight_layout()
        plt.show()
    else:
      return omega_values, hubble_values, omega_final, hubble_final
    
    print(mh_mcmc(0.01,0.01,0,0,10000,plot = False))